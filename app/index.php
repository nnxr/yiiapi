<?php
if (file_exists('./dev.php')) {
    require(__DIR__ . '/dev.php');
}
require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/../config/api.php');

defined('CAPTCHA_FONT_PATH') or define('CAPTCHA_FONT_PATH', __DIR__ . '/captcha.ttf');

(new yii\web\Application($config))->run();
