<?php
$mailSender = 'information@thinkraz.com';
$displayName = '南宁橡芮科技有限公司【开发模式】';
return [
    'class' => 'yii\swiftmailer\Mailer',
    'useFileTransport' => false,
    'transport' => [
        'class' => 'Swift_SmtpTransport',
        'host' => 'smtp.exmail.qq.com',
        'username' => $mailSender,
        'password' => 'ThinkRaz20160329',
        'port' => '465',
        'encryption' => 'SSL',

    ],
    'messageConfig' => [
        'charset' => 'UTF-8',
        'from' => [$mailSender => $displayName]
    ],
];