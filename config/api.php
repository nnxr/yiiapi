<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'zh-CN',
    'timeZone' => 'Asia/Shanghai',
    'modules' => [
        'api' => [
            'class' => 'app\modules\api\Api',
        ],
    ],
    'components' => [
        'i18n' => [
            'translations' => [
                'yii' => [
                    'class' => '\yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => __DIR__ . '\i18n',
                ],
            ],
        ],
        'formatter' => [
            'dateFormat' => 'yyyy-MM-dd',
            'datetimeFormat' => 'yyyy-MM-dd HH:mm:ss',
            'timeFormat' => 'php:H:i:s',
            'decimalSeparator' => '.',
            'thousandSeparator' => ' ',
            'currencyCode' => 'CNY',
        ],
        'request' => [
            'cookieValidationKey' => 'f528764d624db129b32c21fbca0cb8d6',
            'enableCsrfValidation' => false,
            'enableCsrfCookie' => false,
        ],
        'response' => [
            'format' => 'apiJson',
            'formatters' => [
                'apiJson' => 'app\components\ApiJsonResponseFormatter'
            ],
        ],
        'cache' => [
            // 'class' => 'yii\caching\FileCache',
            'class' => 'yii\redis\Cache',
        ],
        'mailer' => YII_ENV_DEV ? require(__DIR__ . '/mail/mail-dev.php') : require(__DIR__ . '/mail/mail.php'),
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/' . '(' . time() . ')' . (md5(json_encode($_SERVER) . json_encode($_REQUEST) . rand(0, 2147483647) . microtime())) . '.log',
//                    'class' => 'yii\log\DbTarget',
                    'levels' => ['error', 'warning', 'trace'],
                    'logVars' => [],
                ],
            ],
        ],
        'db' => YII_ENV_DEV ? require(__DIR__ . '/db/db-dev.php') : require(__DIR__ . '/db/db.php'),
        'redis' => YII_ENV_DEV ? require(__DIR__ . '/redis/redis-dev.php') : require(__DIR__ . '/redis/redis.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'pattern' => '/<controller>/<action>',
                    'route' => 'api/<controller>/<action>',
                ]
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
