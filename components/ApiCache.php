<?php


namespace app\components;


class ApiCache
{
    /**
     * @param $key
     * @return string
     */
    protected static function getKey($key)
    {
        return \Yii::$app->id . '_' . $key;
    }

    /**
     * @param $key
     * @param mixed $value
     * @param int $expire
     * @return bool
     */
    public static function set($key, $value, $expire = 600)
    {

        return \Yii::$app->cache->set(self::getKey($key), $value, $expire);
    }

    /**
     * @param $key
     * @return mixed|false
     */
    public static function get($key)
    {
        return \Yii::$app->cache->get(self::getKey($key));
    }

    /**
     * @param $key
     * @param mixed $value
     * @param int $expire
     * @return bool
     */
    public static function add($key, $value, $expire = 600)
    {
        return \Yii::$app->cache->add(self::getKey($key), $value, $expire);
    }

    /**
     * @param $key
     * @return bool
     */
    public static function delete($key)
    {
        return \Yii::$app->cache->delete(self::getKey($key));
    }
}