<?php


namespace app\components;


use yii\helpers\Json;
use yii\web\JsonResponseFormatter;
use yii\web\Response;

class ApiJsonResponseFormatter extends JsonResponseFormatter
{
    public function formatJson($response)
    {
        $response->getHeaders()->set('Content-Type', 'application/json; charset=UTF-8');
        if ($response->data !== null) {
            $options = $this->encodeOptions;
            if ($this->prettyPrint) {
                $options |= JSON_PRETTY_PRINT;
            }
            $responseData = $this->apiResponseStructure($response, $response->data);
            $response->content = Json::encode($responseData, $options);
            $response->setStatusCode(200);
        }
    }

    /**
     * @param Response $response
     * @param array $data
     *
     * @return array
     */
    private function apiResponseStructure($response, $data)
    {
        $_data = [];
        $_err = null;
        if ($response->getIsServerError() || $response->getIsClientError()) {
            $_err = $data['message'];
            if (YII_ENV_DEV) {
                $_data = $data;
                unset($_data['name']);
                unset($_data['message']);
                unset($_data['code']);
            }
        } else {
            if (is_array($data))
                $_data = $data;
        }
        $_timestamp = $this->millisecondTimestamp();
        $res = [
            'code' => $response->getStatusCode(),
            'status' => strtoupper($response->statusText),
            'data' => $_data,
            'error' => $_err,
            'timestamp' => $_timestamp,
            'sign' => hash_hmac('sha256', Json::encode($_data), $_timestamp),
            'server' => md5(gethostbyname($_SERVER['SERVER_NAME'])),
        ];
        if (YII_ENV_DEV) {
            $res['duration'] = (($this->millisecondTimestamp() - (float)\Yii::getAlias('@_requestStartTimestamp')) / 1000) . 's';
        }
        if ($response->getStatusCode() === 200 && !isset($_err)) {
            unset($res['error']);
        }
        return $res;
    }

    private function millisecondTimestamp()
    {
        list($t1, $t2) = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($t1) + floatval($t2)) * 1000);
    }
}