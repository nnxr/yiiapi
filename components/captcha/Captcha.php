<?php


namespace app\components\captcha;


use app\components\ApiCache;

class Captcha
{
    public static function getCaptcha($expire = 300, $captchaLength = 4, $foreColor = 0x009933, $backColor = 0xFFFFFF, $transparent = true)
    {
        $_t = self::generateToken();
        $captchaHandler = new CaptchaHandler($_t, $expire, $captchaLength, $foreColor, $backColor, $transparent);
        $data = $captchaHandler->getCaptchaBase64Data();
        if (isset($data)) {
            return [
                'captcha' => $data,
                'token' => $_t
            ];
        }
        throw new \Exception('Server is busy');
    }

    public static function validate($token, $code)
    {
        $captchaHandler = new CaptchaHandler($token);
        $valid = $captchaHandler->validateCaptcha($code);
        return [
            'valid' => $valid
        ];
    }

    private static function generateToken()
    {
        $_prefix = '__captcha__';
        $_server = md5(gethostbyname($_SERVER['SERVER_NAME']));
        $_time = self::_generateMsParam();
        $_rnd = rand(0, 2147483647);
        $_str = \Yii::$app->security->generateRandomString(64);
        $token = md5($_prefix . $_server . $_time . $_rnd . $_str);
        if (ApiCache::get($token) === false)
            return $token;
        else
            return self::generateToken();
    }

    private static function _generateMsParam()
    {
        list($t1, $t2) = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($t1) + floatval($t2)) * 1000);
    }
}