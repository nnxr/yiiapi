<?php

namespace app\components\captcha;

use app\components\ApiCache;
use yii\base\InvalidConfigException;
use yii\base\Module;
use yii\captcha\CaptchaAction;
use yii\web\Controller;

class CaptchaHandler extends CaptchaAction
{
    public $token = null;
    public $expire = 0;
    public $foreColor = 0x009933;
    public $backColor = 0xFFFFFF;
    public $transparent = true;
    public $captchaLength = 4;
    public $padding = 6;
    public $height = 50;
    public $offset = 2;
    public $fontFile = CAPTCHA_FONT_PATH;

    public function __construct($token, $expire = 300, $captchaLength = 4, $foreColor = 0x009933, $backColor = 0xFFFFFF, $transparent = true)
    {
        if (!$token) throw new \Exception('Captcha Token cannot be blank');
        $this->foreColor = $foreColor;
        $this->backColor = $backColor;
        $this->transparent = $transparent;
        $this->captchaLength = $captchaLength;
        $id = '_captchaHandler';
        $this->token = $token;
        $this->expire = $expire;
        $controller = new Controller('_captchaController', new Module('_captchaModule'));
        parent::__construct($id, $controller, []);
    }

    public function getCaptchaText()
    {
        return $this->getVerifyCode(true);
    }

    public function getCaptchaBase64Data()
    {
        try {
            $image_data = $this->renderImage($this->getVerifyCode(true));
            $image_info = getimagesizefromstring($image_data);
            return 'data:' . $image_info['mime'] . ';base64,' . base64_encode($image_data);
        } catch (InvalidConfigException $e) {
            return null;
        }
    }

    public function validateCaptcha($captcha_code)
    {
        return $this->validate($captcha_code, false);
    }

    protected function generateVerifyCode()
    {
        $opts = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
        $code = '';
        for ($c = 0; $c < $this->captchaLength; $c++) {
            $code .= $opts[rand(0, 9)];
        }
        return $code;
    }

    public function getVerifyCode($regenerate = false)
    {
        if ($this->fixedVerifyCode !== null) {
            return $this->fixedVerifyCode;
        }
        if (ApiCache::get($this->token) === false) {
            $code = $this->generateVerifyCode();
            ApiCache::set($this->token, $code, $this->expire);
        } else {
            $code = ApiCache::get($this->token);
        }
        return $code;
    }

    public function validate($input, $caseSensitive)
    {
        $code = $this->getVerifyCode();
        $valid = $caseSensitive ? ($input === $code) : strcasecmp($input, $code) === 0;
        if ($valid) {
            ApiCache::delete($this->token);
            return true;
        }
        return false;
    }
}