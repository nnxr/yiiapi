<?php


namespace app\components\utility;

use Yii;
class ApiExcelGenerator
{
    const FORMAT_DATE_YYYYMMDD = '0';
    const FORMAT_NUMBER_COMMA_SEPARATED1 = '1';
    const FORMAT_SET_WIDTH_5 = '2';
    const FORMAT_SET_WIDTH_10 = '3';
    const FORMAT_SET_WIDTH_20 = '4';

    static private function saveExcelToBase64($objPHPExcel, $name)
    {
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        $folderPath = DIRECTORY_SEPARATOR . 'excel';
        $dirPath = \Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . 'web' . $folderPath;
        $filename = $dirPath . DIRECTORY_SEPARATOR . $name . time() . '-' . md5(Yii::$app->security->generateRandomString(32)) . '.xls';
        if (!file_exists($dirPath))
            mkdir($dirPath, 0777, true);
        $objWriter->save($filename);
        $base64 = base64_encode(file_get_contents($filename));
        unlink($filename);

        return $base64;
    }

    static private function ceil($x, $y)
    {
        $columnNames = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if ($x <= 26)
            return substr($columnNames, $x - 1, 1) . $y;
        else {
            $ten = floor(($x - 1) / 26);
            $one = $x % 26;
            return substr($columnNames, $ten - 1, 1) . substr($columnNames, $one - 1, 1) . $y;
        }
    }

    static private function applyStyle($objActSheet, $ceilText, $extra)
    {
        switch ($extra) {
            case self::FORMAT_DATE_YYYYMMDD:
                $objActSheet->getStyle($ceilText)->getNumberFormat()->setFormatCode('yyyy-mm-dd');
                break;
            case self::FORMAT_NUMBER_COMMA_SEPARATED1:
                $objActSheet->getStyle($ceilText)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                break;
            case self::FORMAT_SET_WIDTH_5:
                $objActSheet->getColumnDimension(substr($ceilText, 0, 1))->setWidth(5);
                break;
            case self::FORMAT_SET_WIDTH_10:
                $objActSheet->getColumnDimension(substr($ceilText, 0, 1))->setWidth(10);
                break;
            case self::FORMAT_SET_WIDTH_20:
                $objActSheet->getColumnDimension(substr($ceilText, 0, 1))->setWidth(20);
                break;

        }
    }

    /**
     *  输入数组，导出base64
     *  标准范例：
        $obj =
        [
        'title' => 'filename',
        'contentTitle' => ['title1'],
        'content' => [
            [
            [['text' => 'aaa'], ['text' => 'bbb']],
            [['text' => 'aaa'], ['text' => 'bbb']],
            ]
        ],
        'style' =>
        [[ApiExcelGenerator::FORMAT_SET_WIDTH_20], [ApiExcelGenerator::FORMAT_SET_WIDTH_10]]
        ];
     * @param $obj
     * @return array
     */
    static public function getExcelBase64ByArray($obj)
    {
        set_time_limit(3000);
        $objPHPExcel = new \PHPExcel();
        $filename = $obj['title'];
        $objPHPExcel->getProperties()->setCreator("Bolopie")
            ->setLastModifiedBy("Bolopie")
            ->setTitle($filename)
            ->setSubject($filename)
            ->setDescription("")
            ->setKeywords("")
            ->setCategory("Bolopie auto generated excel");

        for ($c = 0; $c < count($obj['content']); $c++) {
            $content = $obj['content'][$c];
            if ($c > 0) $objPHPExcel->addSheet(new \PHPExcel_Worksheet(), $c);
            $objPHPExcel->setActiveSheetIndex($c);
            $objActSheet = $objPHPExcel->getActiveSheet();
            $objActSheet->setTitle($obj['contentTitle'][$c]);

            for ($i = 0; $i < count($content[0]); $i++) {
                $text = $content[0][$i]['text'];
                $objRichText = new \PHPExcel_RichText();
                $objPayable = $objRichText->createTextRun($text);
                $objPayable->getFont()->setBold(true);
                $objActSheet->setCellValue(self::ceil($i + 1, 1), $objRichText);//期间
            }
            $firstLineCoord = 'A1:' . self::ceil(count($content[0]), 1);
            $objPHPExcel->getActiveSheet()->getStyle($firstLineCoord)->applyFromArray(
                array('fill' => array(
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => 'FFCCFFCC')
                )
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($firstLineCoord)->getAlignment()->setHorizontal(
                \PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            for ($y = 1; $y < count($content); $y++) {
                for ($x = 0; $x < count($content[$y]); $x++) {
                    $text = $content[$y][$x]['text'];
                    $ceil = self::ceil($x + 1, $y + 1);
                    $objActSheet->setCellValue($ceil, $text);
                    if (isset($content[$y][$x]['extra'])) {
                        foreach ($content[$y][$x]['extra'] as $extra) {
                            self::applyStyle($objActSheet, $ceil, $extra);
                        }
                    }

                }
            }
            for ($i = 0; $i < count($obj['style']); $i++) {
                $styles = $obj['style'][$i];
                $columnText = self::ceil($i + 1, 2) . ':' . self::ceil($i + 1, count($content));
                foreach ($styles as $style) {
                    self::applyStyle($objActSheet, $columnText, $style);
                }
            }
        }
        $objPHPExcel->setActiveSheetIndex(0);
        return self::saveExcelToBase64($objPHPExcel, $filename);
    }
}