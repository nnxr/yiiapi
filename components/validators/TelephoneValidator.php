<?php


namespace app\components\validators;


use yii\validators\Validator;

/**
 * Class TelephoneValidator
 * @package app\components\validators
 * 只验证固定电话
 */
class TelephoneValidator extends Validator
{
    private $pattern = '/^(((010)|(021)|(0\d3,4))( ?)([0-9]{7,8}))|((010|021|0\d{3,4}))([- ]{1,2})([0-9]{7,8})$/A';

    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $result = $this->validateValue($model->$attribute);
        if (!empty($result)) {
            $this->addError($model, $attribute, $result[0], $result[1]);
        }
    }

    /**
     * @param mixed $value
     * @return array|null
     */
    public function validateValue($value)
    {
        $errorMessage = 'Invalid telephone format';
        if (preg_match($this->pattern, $value)) {
            return null;
        } else {
            return [$errorMessage, []];
        }
    }
}