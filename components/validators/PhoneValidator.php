<?php


namespace app\components\validators;


use yii\validators\Validator;

/**
 * Class PhoneValidator
 * @package app\components\validators
 * 可验证固话号码和手机号
 */
class PhoneValidator extends Validator
{
    private $cellphonePattern = '/^(?<national>\+?(?:86)?)(?<separator>\s?-?)(?<phone>(?<vender>(13|14|15|18|17)[0-9])(?<area>\d{4})(?<id>\d{4}))$/';
    private $telephonePattern = '/^(((010)|(021)|(0\d3,4))( ?)([0-9]{7,8}))|((010|021|0\d{3,4}))([- ]{1,2})([0-9]{7,8})$/A';

    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $result = $this->validateValue($model->$attribute);
        if (!empty($result)) {
            $this->addError($model, $attribute, $result[0], $result[1]);
        }
    }

    /**
     * @param mixed $value
     * @return array|null
     */
    public function validateValue($value)
    {
        $errorMessage = 'Invalid phone format';
        if (preg_match($this->cellphonePattern, $value) || preg_match($this->telephonePattern, $value)) {
            return null;
        } else {
            return [$errorMessage, []];
        }
    }
}