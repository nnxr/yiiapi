<?php


namespace app\components\validators;


use Exception;
use yii\validators\Validator;

class Base64ImageValidator extends Validator
{
    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $result = $this->validateValue($model->$attribute);
        if (!empty($result)) {
            $this->addError($model, $attribute, $result[0], $result[1]);
        }
    }

    /**
     * @param mixed $value
     * @return array|null
     */
    public function validateValue($value)
    {
        $errorMessage = 'Invalid base64 data received';
        $tmp_arr = explode(',', $value);
        $pure_base64_str = count($tmp_arr) > 1 ? $tmp_arr[1] : $tmp_arr[0];
        if ($pure_base64_str == base64_encode(base64_decode($pure_base64_str))) {
            $binary_image_str = base64_decode($pure_base64_str);
            try {
                imagecreatefromstring($binary_image_str);
                return null;
            } catch (Exception $e) {
                $errorMessage = 'Data is invalid base64 image';
            }
        }
        return [$errorMessage, []];
    }
}