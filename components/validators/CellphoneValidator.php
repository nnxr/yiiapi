<?php


namespace app\components\validators;


use yii\validators\Validator;

/**
 * Class CellphoneValidator
 * @package app\components\validators
 * 只验证手机号
 */
class CellphoneValidator extends Validator
{
    private $pattern = '/^(?<national>\+?(?:86)?)(?<separator>\s?-?)(?<phone>(?<vender>(13|14|15|18|17)[0-9])(?<area>\d{4})(?<id>\d{4}))$/';

    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $result = $this->validateValue($model->$attribute);
        if (!empty($result)) {
            $this->addError($model, $attribute, $result[0], $result[1]);
        }
    }

    /**
     * @param mixed $value
     * @return array|null
     */
    public function validateValue($value)
    {
        $errorMessage = 'Invalid cellphone format';
        if (preg_match($this->pattern, $value)) {
            return null;
        } else {
            return [$errorMessage, []];
        }
    }
}