<?php

namespace app\components\validators;

use yii\helpers\Json;
use yii\validators\Validator;

class JsonValidator extends Validator
{
    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $result = $this->validateValue($model->$attribute);
        if (!empty($result)) {
            $this->addError($model, $attribute, $result[0], $result[1]);
        }
    }

    /**
     * @param mixed $value
     * @return array|null
     */
    public function validateValue($value)
    {
        $errorMessage = 'Data is not Json format';
        try {
            $valid = is_array(Json::decode($value));
        } catch (\Exception $e) {
            $valid = false;
            $errorMessage = 'Json format is invalid';
        }
        if ($valid) return null;
        return [$errorMessage, []];
    }
}