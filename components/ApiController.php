<?php


namespace app\components;


use app\components\model\AccessControlModel;
use app\components\model\ApiModel;
use yii\filters\Cors;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\HttpException;

class ApiController extends Controller
{
    /**
     * @var bool
     */
    public $enableCsrfValidation = false;

    const ACCEPT_GET = 1;
    const ACCEPT_POST = 2;

    /**
     * @var array
     */
    protected $acceptMode = [self::ACCEPT_GET, self::ACCEPT_POST];

    /**
     * @var bool
     */
    protected $enableWhiteList = false;

    /**
     * @var array
     */
    protected $whiteList = [];

    /**
     * @var null|AccessControlModel
     */
    protected $accessControlModelClass = null;

    /**
     * @var bool
     */
    protected $enableCache = true;

    public function init()
    {
        \Yii::setAlias('@_requestStartTimestamp', $this->millisecondTimestamp());
    }

    private function millisecondTimestamp()
    {
        list($t1, $t2) = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($t1) + floatval($t2)) * 1000);
    }

    /**
     * Returns POST parameter with a given name. If name isn't specified, returns an array of all POST parameters.
     *
     * @param string $name the parameter name
     * @param mixed $defaultValue the default parameter value if the parameter does not exist.
     * @return array|mixed
     */
    protected function getPost($name = null, $defaultValue = null)
    {
        return \Yii::$app->request->post($name, $defaultValue);
    }

    /**
     * Returns GET parameter with a given name. If name isn't specified, returns an array of all GET parameters.
     *
     * @param string $name the parameter name
     * @param mixed $defaultValue the default parameter value if the parameter does not exist.
     * @return array|mixed
     */
    protected function getGet($name = null, $defaultValue = null)
    {
        return \Yii::$app->request->get($name, $defaultValue);
    }

    /**
     * @param ApiModel $model
     * @param callable $callback
     * @param int $cacheExpire
     * @param bool $output
     * @param array $acceptMode
     * @return false|null|mixed
     * @throws HttpException
     * @throws \Exception
     */
    protected function handleRequest(ApiModel $model, callable $callback, $cacheExpire = 0, $output = true, $acceptMode = [])
    {
        $_serviceUnavailableText = '暂时无法提供服务';
        $_acceptModeEmptyText = '无法接受任何协议的请求';
        $_requestParamsLoadFailedText = '请求参数加载错误';
        $_invalidRequestProtocolText = '非法的请求协议';
        $_dataVerifyFailedText = '请求数据验证失败';

        $getParams = $this->getGet();
        $postParams = $this->getPost();
        $modelName = $model::className();
        $uniqueId = $this->action->uniqueId;
        $cacheToken = md5(Json::encode($getParams) . Json::encode($postParams) . $modelName . $uniqueId . $cacheExpire);
        $cacheData = $cacheExpire > 0 ? ($this->enableCache ? ApiCache::get($cacheToken) : false) : false;
        if ($cacheData === false) {
            $__acceptMode = $this->acceptMode;
            if (is_array($acceptMode)) {
                if (count($acceptMode) > 0)
                    $this->acceptMode = $acceptMode;
            }
            if (!is_array($this->acceptMode))
                throw new \Exception($_serviceUnavailableText);
            if (count($this->acceptMode) === 0)
                throw new \Exception($_acceptModeEmptyText);
            foreach ($this->acceptMode as $_acceptMode) {
                switch ($_acceptMode) {
                    case self::ACCEPT_GET: {
                        if (!$model->load($getParams, ''))
                            throw new HttpException(403, $_requestParamsLoadFailedText);
                    }
                        break;
                    case self::ACCEPT_POST: {
                        if (!$model->load($postParams, ''))
                            throw new HttpException(403, $_requestParamsLoadFailedText);
                    }
                        break;
                    default: {
                        throw new HttpException(403, $_invalidRequestProtocolText);
                    }
                }
            }
            $this->acceptMode = $__acceptMode;
            if (!$model->validate())
                if (YII_ENV_DEV)
                    throw new HttpException(403, Json::encode($model->getErrors()));
                else
                    throw new HttpException(403, $_dataVerifyFailedText);
            $data = $callback($model);
            if ($cacheExpire > 0 && $this->enableCache) {
                ApiCache::set($cacheToken, $data, $cacheExpire);
            }
        } else {
            if ($cacheExpire === 0)
                ApiCache::delete($cacheToken);
            $data = $cacheData;
        }
        if ($output) {
            \Yii::$app->response->data = isset($data) ? $data : [];
            \Yii::$app->response->send();
            exit(0);
        }
        return $data;
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws HttpException
     */
    public function beforeAction($action)
    {
        if ($this->enableWhiteList) {
            $permissionDenyText = '无访问权限';
            $serviceUnavailableText = '暂时无法提供服务';
            if (!in_array($action->id, $this->whiteList))
                throw new HttpException(403, $permissionDenyText);
            if (!isset($this->accessControlModelClass))
                throw new HttpException(500, $serviceUnavailableText);
            $reflectionClass = new \ReflectionClass($this->accessControlModelClass);
            if (!$reflectionClass->isInstantiable())
                throw new HttpException(500, $serviceUnavailableText);
            $accessControlModel = new $this->accessControlModelClass();
            if (!($accessControlModel instanceof AccessControlModel)) {
                throw new HttpException(500, $serviceUnavailableText);
            }
            $validAccess = $this->handleRequest($accessControlModel, function (AccessControlModel $model) {
                return $model->accessValidate();
            }, 600, false);
            if (!$validAccess) throw new HttpException(403, $permissionDenyText);
        }
        return parent::beforeAction($action);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST', 'GET'],
                ],

            ],
        ];
    }
}