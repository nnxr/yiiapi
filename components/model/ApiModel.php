<?php

namespace app\components\model;

use ArrayObject;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\validators\Validator;

class ApiModel extends Model
{
    private $appendValidators = [
        'json' => 'app\components\validators\JsonValidator',
        'base64Image' => 'app\components\validators\Base64ImageValidator',
        'cellphone' => 'app\components\validators\CellphoneValidator',
        'telephone' => 'app\components\validators\TelephoneValidator',
        'phone' => 'app\components\validators\PhoneValidator',
    ];

    protected $errorMessage = '发生未知错误';

    /**
     * @param array $data
     * @param null $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        if (empty($data)) return true;
        $scope = $formName === null ? $this->formName() : $formName;
        if ($scope === '' && !empty($data)) {
            $this->setAttributes($data);
            return true;
        } elseif (isset($data[$scope])) {
            $this->setAttributes($data[$scope]);
            return true;
        }
        return false;
    }

    /**
     * @param $errorMessage
     */
    protected function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @return ArrayObject
     * @throws InvalidConfigException
     */
    public function createValidators()
    {
        Validator::$builtInValidators = ArrayHelper::merge(Validator::$builtInValidators, $this->appendValidators);
        $validators = new ArrayObject;
        foreach ($this->rules() as $rule) {
            if ($rule instanceof Validator) {
                $validators->append($rule);
            } elseif (is_array($rule) && isset($rule[0], $rule[1])) { // attributes, validator type
                $validator = Validator::createValidator($rule[1], $this, (array)$rule[0], array_slice($rule, 2));
                $validators->append($validator);
            } else {
                throw new InvalidConfigException('Invalid validation rule: a rule must specify both attribute names and validator type.');
            }
        }
        return $validators;
    }

    /**
     * @param null $attributeNames
     * @param bool $clearErrors
     * @return bool
     */
    public function validate($attributeNames = null, $clearErrors = true)
    {
        if ($clearErrors) {
            $this->clearErrors();
        }

        if (!$this->beforeValidate()) {
            return false;
        }

        $scenarios = $this->scenarios();
        $scenario = $this->getScenario();
        if (!isset($scenarios[$scenario])) {
            throw new InvalidParamException("Unknown scenario: $scenario");
        }

        if ($attributeNames === null) {
            $attributeNames = $this->activeAttributes();
        }

        foreach ($this->getActiveValidators() as $validator) {
            $validator->validateAttributes($this, $attributeNames);
        }
        $afterValidateResult = $this->afterValidate();
        if (isset($afterValidateResult)) {
            if (!$afterValidateResult) {
                $this->addError('validation', 'validation is failed');
            }
        }
        return !$this->hasErrors();
    }
}