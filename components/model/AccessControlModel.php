<?php

namespace app\components\model;

class AccessControlModel extends ApiModel
{
    protected $errorMessage = '无访问权限';

    /**
     * @return boolean
     */
    public function accessValidate()
    {
        return false;
    }

    /**
     * @param string $message
     */
    protected function setDenyMessage($message)
    {
        $this->errorMessage = $message;
    }

    /**
     * @return string
     */
    public function getDenyMessage()
    {
        return $this->errorMessage;
    }
}