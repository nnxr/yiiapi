<?php


namespace app\components\schema;


use app\components\schema\ar\IntegerAR;
use Yii;
use yii\base\Exception;

class Integer extends BaseSchema
{
    public function activeRecord()
    {
        return IntegerAR::class;
    }

    public function parse($value)
    {
        return (integer)$value;
    }

    public function format($value)
    {
        return (integer)$value;
    }

    protected function createTable($tableName)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            Yii::$app->db->createCommand()->createTable($tableName, [
                '_id' => 'bigpk',
                '_batch' => 'varchar(32) NOT NULL',
                '_hash' => 'varchar(32) NOT NULL',
                'key' => 'varchar(256) NOT NULL',
                'value' => 'bigint(20) NOT NULL',
            ])->execute();
            Yii::$app->db->createCommand()->createIndex('b_idx', $tableName, '_batch')->execute();
            Yii::$app->db->createCommand()->createIndex('h_idx', $tableName, '_hash')->execute();
            Yii::$app->db->createCommand()->createIndex('k_idx', $tableName, 'key')->execute();
            Yii::$app->db->createCommand()->createIndex('v_idx', $tableName, 'value')->execute();
            $transaction->commit();
            return true;
        } catch (Exception $e) {
            $transaction->rollBack();
            throw new Exception('table create failed');
        }
    }
}