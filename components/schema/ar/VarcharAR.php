<?php

namespace app\components\schema\ar;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "Text".
 *
 * @property integer $_id
 * @property string $_batch
 * @property string $_hash
 * @property string $key
 * @property string $value
 */
class VarcharAR extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'varchar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_batch', '_hash', 'key', 'value'], 'required'],
            [['_id'], 'integer'],
            [['_batch', '_hash', 'key'], 'string'],
            [['value'], 'string', 'max' => 1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => '编号',
            '_batch' => '批次',
            '_hash' => '哈希',
            'key' => '键名',
            'value' => '值',
        ];
    }
}