<?php

namespace app\components\schema\ar;

use yii\db\ActiveRecord;
/**
 * This is the model class for table "Integer".
 *
 * @property integer $_id
 * @property string $_batch
 * @property string $_hash
 * @property string $key
 * @property integer $value
 */
class IntegerAR extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'integer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_batch', '_hash', 'key', 'value'], 'required'],
            [['_id'], 'integer'],
            [['_batch', '_hash', 'key'], 'string'],
            [['value'], 'integer' ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => '编号',
            '_batch' => '批次',
            '_hash' => '哈希',
            'key' => '键名',
            'value' => '值',
        ];
    }
}