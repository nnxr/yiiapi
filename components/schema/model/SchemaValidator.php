<?php


namespace app\components\schema\model;


use app\components\model\ApiModel;

class SchemaValidator extends ApiModel
{
    public $input;

    public $validatorName = null;

    public function rules()
    {
        return [
            [['input', 'validatorName'], 'required'],
            ['validatorName', 'safe'],
            ['input', $this->validatorName],
        ];
    }
}