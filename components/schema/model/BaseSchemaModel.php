<?php


namespace app\components\schema\model;


use app\components\schema\BaseSchema;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\Json;

class BaseSchemaModel
{
    /**
     * @var array
     */
    public $data = [];

    private $where = [];
    private $orderBy = [];
    private $limit = null;
    private $offset = null;
    private $rawQuery = [];
    private $rawBatchId = [];
    private $rawData = [];

    private $conversionKey = '__________fields';

    /**
     * BaseSchemaModel constructor.
     * @param array $data
     */
    public function __construct($data = [])
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function schema()
    {
        return [];
    }

    /**
     * @return string
     */
    public function UniqueHash()
    {
        return md5(self::class);
    }

    /**
     * @param array $validateArray
     * @param array $dataArray
     * @param string $batchId
     * @param string $keyStr
     * @return bool|array
     */
    private function internalSave($validateArray, $dataArray, $batchId, $keyStr = '')
    {
        $resultArray = [];
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            foreach ($validateArray as $key => $value) {
                $_keyStr = $keyStr . $key;
                if ($value instanceof BaseSchema) {
                    //not array
                    if (isset($dataArray[$key])) {
                        if (!$value->validate($dataArray[$key])) throw new \Exception('Data type error');
                        $hash = md5($this->UniqueHash() . $_keyStr);
                        if (!$value->insert($batchId, $hash, $key, $dataArray[$key])) throw new \Exception('Database save error');
                        $resultArray[$key] = $dataArray[$key];
                    } else {
                        if (!$value->allowNull()) throw new \Exception('Input data structure error');
                        $resultArray[$key] = null;
                    }
                } else {
                    //array
                    if (!is_array($dataArray[$key])) throw new \Exception('Data schema is not matched');
                    $subResultArray = $this->internalSave($value, $dataArray[$key], $batchId, $_keyStr);
                    if ($subResultArray === false) throw new \Exception('Sub data save error');
                    $resultArray[$key] = $subResultArray;
                }
            }
            $transaction->commit();
            return $resultArray;
        } catch (\Exception $e) {
            $transaction->rollBack();
            return false;
        }
    }

    /**
     * @param $keyStr
     * @return string
     * @throws \Exception
     */
    private function KeyToHash($keyStr)
    {
        $arr = explode(".", $keyStr);
        $structure = $this->schema();
        $keyStr = '';
        foreach ($arr as $key) {
            if (!isset($structure[$key])) throw new \Exception('Key is not exist');
            $structure = $structure[$key];
            $keyStr .= $key;
        }
        return md5($this->UniqueHash() . $keyStr);
    }

    private function formatRawData(array $rawDataArray, array $sequenceArray = [])
    {
        $templateArray = $this->getTemplateArray($this->schema());
        $_tmpCache = [];
        foreach ($rawDataArray as $rawData) {
            if (!isset($_tmpCache[$rawData['_batch']])) {
                $_tmpCache[$rawData['_batch']] = array_merge([], $templateArray);
            }
            $_tmp = &$_tmpCache[$rawData['_batch']];
            $conversionKey = $this->conversionKey;
            $this->replaceArrayValue($_tmpCache[$rawData['_batch']], $rawData['_hash'], $rawData['value'],
                function ($fieldStr) use (&$_tmp, $conversionKey) {
                    unset($_tmp[$conversionKey][array_search($fieldStr, $_tmp[$conversionKey])]);
                });
        }
        $distArray = $sequenceArray;
        foreach ($_tmpCache as $batchKey => $objArray) {
            if (count($objArray[$this->conversionKey]) > 0) {
                foreach ($objArray[$this->conversionKey] as $field) {
                    $_arr = explode(".", $field);
                    $_cache =& $objArray;
                    foreach ($_arr as $key) {
                        $_cache =& $_cache[$key];
                    }
                    if (!is_array($_cache))
                        $_cache = null;
                }
            }
            unset($objArray[$this->conversionKey]);
            $batchIndex = array_search($batchKey, $distArray);
            if ($batchIndex === false) {
                array_push($distArray, $objArray);
            } else {
                $distArray[$batchIndex] = $objArray;
            }
        }
        return $distArray;
    }

    /**
     * @param array $array
     * @param $search
     * @param $replace
     * @param callable $callback
     * @param string $keyStr
     * @return bool
     */
    private function replaceArrayValue(array &$array, $search, $replace, callable $callback, $keyStr = '')
    {
        foreach ($array as $key => $value) {
            if (strlen($keyStr) > 0) {
                $_keyStr = $keyStr . '.' . $key;
            } else {
                $_keyStr = $key;
            }
            if (!is_array($value)) {
                if ($value == $search) {
                    $array[$key] = $replace;
                    $callback($_keyStr);
                    return true;
                }
            } else {
                if ($this->replaceArrayValue($array[$key], $search, $replace, $callback, $_keyStr)) return true;
            }
        }
        return false;
    }

    /**
     * @param array $schemaArray
     * @param string $keyStr
     * @return array
     */
    private function getTemplateArray(array $schemaArray, $keyStr = '')
    {
        $templateArray = [];
        $fields = [];
        foreach ($schemaArray as $key => $value) {
            $_keyStr = $keyStr . $key;
            if (strlen($keyStr) > 0) {
                $_fields = $keyStr . '.' . $key;
            } else {
                $_fields = $key;
            }
            array_push($fields, $_fields);
            if ($value instanceof BaseSchema) {
                //not array
                $templateArray[$key] = md5($this->UniqueHash() . $_keyStr);
            } else {
                //array
                $templateArray[$key] = $this->getTemplateArray($schemaArray[$key], $_keyStr);
                $fields = array_merge($templateArray[$key][$this->conversionKey], $fields);
                unset($templateArray[$key][$this->conversionKey]);
            }
        }
        $templateArray[$this->conversionKey] = $fields;
        return $templateArray;
    }

    /**
     * @return string
     */
    private function generateBatchToken()
    {
        list($t1, $t2) = explode(' ', microtime());
        $_timestamp = (float)sprintf('%.0f', (floatval($t1) + floatval($t2)) * 1000);
        return md5(self::class . $_timestamp . rand(0, 2147483646) . \Yii::$app->security->generateRandomString());
    }

    /**
     * @return bool|array
     */
    public function save()
    {
        $batchToken = $this->generateBatchToken();
        return $this->internalSave($this->schema(), $this->getData(), $batchToken);
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * 开始查询
     * @return $this
     */
    public static function find()
    {
        $class = get_called_class();
        return new $class();
    }

    /**
     * @param  string|array $condition
     * @return $this
     */
    public function where($condition)
    {
        $this->where = [];
        array_push($this->where, ['', $condition]);
        return $this;
    }

    /**
     * @param  string|array $condition
     * @return $this
     */
    public function orderBy($condition)
    {
        $this->orderBy = $condition;
        return $this;
    }

    /**
     * @param integer $condition
     * @return $this
     */
    public function limit($condition)
    {
        $this->limit = $condition;
        return $this;
    }

    /**
     * @param integer $condition
     * @return $this
     */
    public function offset($condition)
    {
        $this->offset = $condition;
        return $this;
    }

    /**
     * @param $condition array
     * @return $this
     */
    public function andWhere($condition)
    {
        array_push($this->where, ['AND', $condition]);
        return $this;
    }

    /**
     * @param $condition array
     * @return $this
     */
    public function orWhere($condition)
    {
        array_push($this->where, ['OR', $condition]);
        return $this;
    }

    /**
     * 输入：['id'=>2] 或者 ['IN','id', 'value'] 的形式
     * 输出：如果为后者则为true
     * @param $condition
     * @return bool
     */
    private function checkHasKeyValue($condition)
    {
        return array_keys($condition)[0] === 0;
    }

    /**
     * @param $keyStr
     * @return ActiveRecord
     * @throws \Exception
     */
    private function KeyToAR($keyStr)
    {
        $arr = explode(".", $keyStr);
        $structure = $this->schema();
        foreach ($arr as $key) {
            if (!isset($structure[$key])) throw new \Exception('Key is not exist');
            $structure = $structure[$key];
        }
        return $structure->activeRecord;
    }

    private function flattenArray($array, $key)
    {
        return array_column($array, $key);
    }

    private function calculateWhere($updateCondition = [])
    {
        $rawData = [];
        foreach ($this->where as $item) {
            $condition = $item[1];
            if ($this->checkHasKeyValue($condition)) {
                //TODO
                if (count($condition) < 3) throw new \Exception('condition shorter then 3 is not supported');
                $ar = $this->KeyToAR($condition[1]);
                $hash = $this->KeyToHash($condition[1]);
                $queryResult = $ar::find()
                    ->where(['_hash' => $hash])
                    ->andWhere([$condition[0], 'value', $condition[2]]);
                if (count($rawData) > 0) {
                    $queryResult = $queryResult->andWhere(['in', '_batch', $rawData]);
                }
                $queryResult = $queryResult->select(['_batch'])->asArray()->all();
                $rawData = $this->flattenArray($queryResult, '_batch');
                //TODO 如果要支持or,whereCounter不能包含同样列
            } else {
                foreach ($condition as $key => $value) {
                    $ar = $this->KeyToAR($key);
                    $hash = $this->KeyToHash($key);
                    $queryResult = $ar::find()->where(['_hash' => $hash, 'value' => $value]);
                    if (count($rawData) > 0) {
                        $queryResult = $queryResult->andWhere(['in', '_batch', $rawData]);
                    }
                    $queryResult = $queryResult->select(['_batch'])->asArray()->all();
                    $rawData = $this->flattenArray($queryResult, '_batch');
                    //TODO 如果要支持or,whereCounter不能包含同样列
                }
            }
            switch ($item[0]) {
                case '':
//                    $query = $query->where($condition);
                    break;
                case 'AND':
//                    $query = $query->andWhere($condition);
                    break;
                case 'OR':
//                    $query = $query->orWhere($condition);
                    break;
            }
        }
        $rawData = $this->calculateOrderBy($rawData);
        if(count($updateCondition)>0) $this->calculateUpdate($updateCondition,$rawData);
        $this->queryRawData($this->schema(), '');
        foreach ($this->rawQuery as $value) {
            $ar = $value['ar'];
            $hash = $value['hash'];
            $data = $ar::find()
                ->where(['in', '_batch', $rawData])
                ->andWhere(['in', '_hash', $hash])
                ->asArray()
                ->all();
            $this->rawData = array_merge($this->rawData, $data);
        }
        $this->rawBatchId = $rawData;
//        die(json_encode($this->rawData));
        return count($this->orderBy) > 0 ? $rawData : [];
    }

    private function calculateOrderBy($batchArray)
    {
        if (count($this->orderBy) > 1) throw new Exception('Currently orderBy only supports 1 condition');
        foreach ($this->orderBy as $key => $sort) {
            $ar = $this->KeyToAR($key);
            $hash = $this->KeyToHash($key);
            $data = $ar::find()
                ->where(['in', '_batch', $batchArray])
                ->andWhere(['_hash' => $hash])
                ->orderBy(['value' => $sort])
                ->limit($this->limit)
                ->offset($this->offset)
                ->select(['_batch'])
                ->asArray()
                ->all();
            return array_column($data, '_batch');
        }
        return $batchArray;
    }

    private function queryRawData($schema, $keyName)
    {
        foreach ($schema as $key => $value) {
            if (is_array($value)) {
                $this->queryRawData($value, $keyName == '' ? $key : $keyName . '.' . $key);
            } else {
                $_key = $keyName == '' ? $key : $keyName . '.' . $key;
                $hash = $this->KeyToHash($_key);
                $ar = $this->KeyToAR($_key);
                $arName = $ar::className();
                if (!isset($this->rawQuery[$arName])) {
                    $this->rawQuery[$arName] = [
                        'ar' => $ar,
                        'hash' => []
                    ];
                }
                array_push($this->rawQuery[$arName]['hash'], $hash);
            }
        }
    }

    private function calculateUpdate($condition,$batchArray)
    {
        foreach ($condition as $key => $value){
            $hash = $this->KeyToHash($key);
            $ar = $this->KeyToAR($key);
            $ar::updateAll(['value'=>$value],['and',
                ['IN','_batch',$batchArray],
                ['IN','_hash',$hash],
            ]);
        }
    }

    public function all()
    {
        $data = $this->calculateWhere();
        return $this->formatRawData($this->rawData, $data);
    }

    public function update($condition = [])
    {
        $data = $this->calculateWhere($condition);
        return $this->formatRawData($this->rawData, $data);
    }
}