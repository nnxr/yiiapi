<?php


namespace app\components\schema;


use app\components\schema\ar\TextAR;
use Exception;
use Yii;

class Text extends BaseSchema
{
    public function activeRecord()
    {
        return TextAR::class;
    }

    public function parse($value)
    {
        return (string)$value;
    }

    public function format($value)
    {
        return (string)$value;
    }

    protected function createTable($tableName)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            Yii::$app->db->createCommand()->createTable($tableName, [
                '_id' => 'bigpk',
                '_batch' => 'varchar(32) NOT NULL',
                '_hash' => 'varchar(32) NOT NULL',
                'key' => 'varchar(256) NOT NULL',
                'value' => 'longtext NOT NULL',
            ])->execute();
            Yii::$app->db->createCommand()->createIndex('b_idx', $tableName, '_batch')->execute();
            Yii::$app->db->createCommand()->createIndex('h_idx', $tableName, '_hash')->execute();
            Yii::$app->db->createCommand()->createIndex('k_idx', $tableName, 'key')->execute();
            Yii::$app->db->createCommand('ALTER TABLE `'.$tableName.'` ADD FULLTEXT INDEX v_idx (`value`)')->execute();

            $transaction->commit();
            return true;
        } catch (Exception $e) {
            $transaction->rollBack();
            throw new Exception($e->getMessage());
        }
    }
}