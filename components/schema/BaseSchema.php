<?php

namespace app\components\schema;

use app\components\schema\model\SchemaValidator;
use yii\db\ActiveRecord;

class BaseSchema
{
    /**
     * @var bool
     */
    public $allowNull = false;

    public $dataType = null;

    /**
     * @var ActiveRecord
     */
    public $activeRecord = null;

    /**
     * BaseSchema constructor.
     * @param string $dataType
     * @param bool $allowNull
     */
    public function __construct($dataType, $allowNull = false)
    {
        $this->activeRecord = $this->activeRecord();
        $this->dataType = $dataType;
        $this->allowNull = $allowNull;
        $this->prepareTable();
    }

    /**
     * @return ActiveRecord
     */
    public function activeRecord()
    {
        return null;
    }

    /**
     * @param string $dataType
     * @param bool $allowNull
     * @return BaseSchema
     */
    public static function schema($dataType, $allowNull = false)
    {
        $class = get_called_class();
        return (new $class($dataType, $allowNull));
    }

    /**
     * @param $data
     * @return bool
     */
    public function validate($data)
    {
        $validator = new SchemaValidator([
            'input' => $data,
            'validatorName' => $this->dataType
        ]);
        return $validator->validate();
    }

    /**
     * @return string
     */
    public function className()
    {
        return self::class;
    }

    /**
     * @param string $batchToken
     * @param string $hash
     * @param string $key
     * @param $value
     * @return bool
     */
    public function insert($batchToken, $hash, $key, $value)
    {
        $orm = new $this->activeRecord([
            '_batch' => $batchToken,
            '_hash' => $hash,
            'key' => $key,
            'value' => $value
        ]);
        if ($orm->save()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $value
     * @return mixed
     */
    public function parse($value)
    {
        return $value;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function format($value)
    {
        return $value;
    }

    /**
     * @return ActiveRecord
     * @throws \Exception
     */
    public function getORM()
    {
        if (!isset($this->activeRecord)) throw new \Exception('Active Record Class has not defined');
        return $this->activeRecord;
    }

    /**
     * @throws \BadMethodCallException
     */
    protected function createTable($tableName)
    {
        throw new \BadMethodCallException('Cannot use this method in BaseSchema class');
    }

    /**
     * @return void
     */
    public function prepareTable()
    {
        $tables = \Yii::$app->db->getSchema()->getTableNames();
        if (!in_array($this->activeRecord::tableName(), $tables)) {
            $this->createTable($this->activeRecord::tableName());
        }
    }

    /**
     * @return bool
     */
    public function allowNull()
    {
        return $this->allowNull;
    }
}