<?php


namespace app\modules\Api\models;


use app\components\model\ApiModel;

class TestModel extends ApiModel
{
    public function add()
    {
        $gender = ['male', 'female'];
        $model = new TesterModel([
            'id' => (string)rand(1000000, 9999999),
            'name' => \Yii::$app->security->generateRandomString(3),
            'info' => [
                'age' => rand(1, 99),
                'gender' => $gender[rand(0, 1)],
                'intro' => \Yii::$app->security->generateRandomString(1024),
                'died' => rand(0, 1),
            ]
        ]);
        return $model->save();
    }

    public function find()
    {
//        $model = new TesterModel();
//        return [$model->KeyToAR('info.gender')];
        return TesterModel::find()
            ->where(['info.age' => 42])
//            ->orderBy(['info.age' => SORT_DESC])
            ->all();
    }
}