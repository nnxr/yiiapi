<?php


namespace app\modules\Api\models;


use app\components\schema\Boolean;
use app\components\schema\Integer;
use app\components\schema\model\BaseSchemaModel;
use app\components\schema\Text;
use app\components\schema\Varchar;

class TesterModel extends BaseSchemaModel
{
    public function schema()
    {
        return [
            'id' => Varchar::schema('integer'),
            'name' => Varchar::schema('string'),
            'info' => [
                'age' => Integer::schema('integer'),
                'gender' => Varchar::schema('string'),
                'intro' => Text::schema('string'),
                'died' => Boolean::schema('integer', true),
            ]
        ];
    }
}