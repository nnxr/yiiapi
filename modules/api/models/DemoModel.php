<?php

namespace app\modules\Api\models;

use app\components\captcha\Captcha;
use app\components\model\ApiModel;

class DemoModel extends ApiModel
{
    public $token;
    public $code;
    public $test;

    public function rules()
    {
        return [
            ['token', 'string'],
            ['code', 'integer'],
            ['test', 'safe'],
        ];
    }

    public function getCaptcha()
    {
        return Captcha::getCaptcha();
    }

    public function validateCaptcha()
    {
        return Captcha::validate($this->token, $this->code);
    }
}