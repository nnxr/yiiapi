<?php

namespace app\modules\Api;

use yii\base\Module;

class Api extends Module
{
    public $controllerNamespace = 'app\modules\Api\controllers';
    public $defaultRoute = null;

    public function init()
    {
        parent::init();
    }
}
