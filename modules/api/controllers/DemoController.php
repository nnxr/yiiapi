<?php

namespace app\modules\Api\controllers;

use app\components\ApiController;
use app\components\utility\ApiExcelGenerator;
use app\modules\Api\models\DemoModel;
use app\modules\Api\models\TestModel;

class DemoController extends ApiController
{
    public function actionGetCaptcha()
    {
        $this->handleRequest(new DemoModel(), function (DemoModel $model) {
            return $model->getCaptcha();
        });
    }

    public function actionValidateCaptcha()
    {
        $this->handleRequest(new DemoModel(), function (DemoModel $model) {
            return $model->validateCaptcha();
        });
    }

    public function actionAdd()
    {
        $this->handleRequest(new TestModel(), function (TestModel $model) {
            return $model->add();
        });
    }

    public function actionFind()
    {
        $this->handleRequest(new TestModel(), function (TestModel $model) {
            return $model->find();
        });
    }
}
